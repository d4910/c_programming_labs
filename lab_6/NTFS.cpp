#include "main.h"

NTFS::NTFS(UnicodeString diskLabel) : FileSystem(diskLabel, 0, 0, 512) {
    byte_ptr bootRecord = new BYTE[512];
	UINT64 readSuccess = this->ReadCluster(0, 1, bootRecord);
	if (readSuccess) {
		NTFS_BootRecord* pNTFS_BootRecord = reinterpret_cast<NTFS_BootRecord*&>(bootRecord);
		ULONGLONG OEM_Name = pNTFS_BootRecord->OEM_Name;
		if (OEM_Name == 0x202020205346544E) {

			this->ClusterSize = pNTFS_BootRecord->BytesPerSector *  pNTFS_BootRecord->SectorsPerCluster;

			this->FSClustersAmount = pNTFS_BootRecord->SectorsCount;

			this->FSSize = this->getClusterSize() * this->getFSClustersAmount();
		}
		else std::cout << "This is not an NTFS boot record! Exit." << std::endl;
	}
}
