object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 188
  ClientWidth = 285
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 94
    Width = 46
    Height = 13
    Caption = 'Read size'
  end
  object Label2: TLabel
    Left = 42
    Top = 132
    Width = 44
    Height = 13
    Caption = 'Disk label'
  end
  object Button1: TButton
    Left = 40
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 40
    Top = 63
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 121
    Top = 94
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '512'
  end
  object Edit2: TEdit
    Left = 121
    Top = 129
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'D'
  end
end
