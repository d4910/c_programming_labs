object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Muratova LR4'
  ClientHeight = 243
  ClientWidth = 415
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 8
    Top = 208
    Width = 211
    Height = 19
    Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088' '#1090#1080#1087#1072' '#1084#1077#1076#1080#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 225
    Top = 208
    Width = 5
    Height = 19
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object VirtualStringTree1: TVirtualStringTree
    Left = 8
    Top = 8
    Width = 265
    Height = 194
    Header.AutoSizeIndex = 0
    Header.Options = [hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible]
    TabOrder = 0
    OnAddToSelection = VirtualStringTree1AddToSelection
    OnGetText = VirtualStringTree1GetText
    Touch.InteractiveGestures = [igPan, igPressAndTap]
    Touch.InteractiveGestureOptions = [igoPanSingleFingerHorizontal, igoPanSingleFingerVertical, igoPanInertia, igoPanGutter, igoParentPassthrough]
    Columns = <
      item
        Position = 0
        Text = #1058#1080#1087' '#1084#1077#1076#1080#1072
        Width = 209
      end>
  end
  object Button1: TButton
    Left = 279
    Top = 8
    Width = 121
    Height = 34
    Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1076#1072#1085#1085#1099#1077
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 279
    Top = 48
    Width = 121
    Height = 34
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1076#1072#1085#1085#1099#1077
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 279
    Top = 88
    Width = 121
    Height = 34
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 279
    Top = 128
    Width = 121
    Height = 34
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077' '#1089#1090#1088#1086#1082#1080
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 279
    Top = 168
    Width = 121
    Height = 34
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1074#1080#1089#1080#1084#1099#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = Button5Click
  end
end
