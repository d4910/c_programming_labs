//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VirtualTrees"
#pragma resource "*.dfm"
TForm1 *Form1;
sqlite3* Database;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	VirtualStringTree1->NodeDataSize = sizeof(node);
	int db_not_opened = sqlite3_open("Chinook_Sqlite.sqlite", &Database);
	if (db_not_opened)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"���� ������ �� �������!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	VirtualStringTree1->Clear();
	VirtualStringTree1->BeginUpdate();

	sqlite3_stmt *pStatement;
	int result = sqlite3_prepare_v2(Database,"SELECT Name FROM MediaType",-1,&pStatement,NULL);
	result = sqlite3_step(pStatement);

	if(result != SQLITE_ROW)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"������ �� ���������� �� ��������!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	while(true)
	{
		if(result != SQLITE_ROW) break;

		char *media_type = sqlite3_column_text(pStatement, 0);

		PVirtualNode entry_node = VirtualStringTree1->AddChild(VirtualStringTree1->RootNode);
		node *node_data = (node*)VirtualStringTree1->GetNodeData(entry_node);

		node_data->media_type = media_type;
        result = sqlite3_step(pStatement);
	}

	VirtualStringTree1->EndUpdate();
	sqlite3_finalize(pStatement);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::VirtualStringTree1GetText(TBaseVirtualTree *Sender, PVirtualNode Node,
		  TColumnIndex Column, TVSTTextType TextType, UnicodeString &CellText)

{
	if (!Node)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"������ ��������� ����!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	node *node_data = (node*)VirtualStringTree1->GetNodeData(Node);
	switch (Column)
	{
		case 0:
		{
			CellText = node_data->media_type; break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::VirtualStringTree1AddToSelection(TBaseVirtualTree *Sender,
          PVirtualNode Node)
{
	if (!Node)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"������ ��������� ����!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	node *node_data = (node*)VirtualStringTree1->GetNodeData(Node);

	sqlite3_stmt *pStatement;

	UnicodeString unicode_sql = L"SELECT MediaTypeId FROM MediaType WHERE Name=\""+node_data->media_type+"\"";

	wchar_t *wchar_sql = unicode_sql.c_str();
	char char_sql[200];
	wcstombs(char_sql, wchar_sql, wcslen(wchar_sql)+1);

	int result = sqlite3_prepare_v2(Database,char_sql,-1,&pStatement,NULL);
	result = sqlite3_step(pStatement);

	if(result != SQLITE_ROW)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"������ ��������� �������������� ���� �����!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	const unsigned char *media_type_id = sqlite3_column_text(pStatement, 0);
	Label2->Caption = UnicodeString((char*)media_type_id);
	sqlite3_finalize(pStatement);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button2Click(TObject *Sender)
{
	VirtualStringTree1->Clear();
	Label2->Caption = "";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	sqlite3_stmt *pStatement;

	PVirtualNode selected_node = VirtualStringTree1->FocusedNode;
	node *node_data = (node*)VirtualStringTree1->GetNodeData(selected_node);

	UnicodeString unicode_sql = "DELETE FROM MediaType WHERE Name=\""+node_data->media_type+"\"";

	wchar_t *wchar_sql = unicode_sql.c_str();
	char char_sql[100];
	wcstombs(char_sql, wchar_sql, wcslen(wchar_sql)+1);

	int result = sqlite3_prepare_v2(Database,char_sql,-1,&pStatement,NULL);
	result = sqlite3_step(pStatement);

	if(result != SQLITE_DONE)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"�������� ������ �� ���������!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	sqlite3_finalize(pStatement);
	VirtualStringTree1->DeleteNode(selected_node);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	sqlite3_stmt *pStatement;
	int result = sqlite3_prepare_v2(Database,"DELETE FROM MediaType;",-1,&pStatement,NULL);
	result = sqlite3_step(pStatement);

	if(result != SQLITE_DONE)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"�������� ���� ������ �� ���������!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	sqlite3_finalize(pStatement);
	VirtualStringTree1->Clear();
}
//---------------------------------------------------------------------------



void __fastcall TForm1::Button5Click(TObject *Sender)
{
	sqlite3_stmt *pStatement1;

	PVirtualNode selected_node = VirtualStringTree1->FocusedNode;

	if (!selected_node) {
        int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"������ �� ��������!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	node *node_data = (node*)VirtualStringTree1->GetNodeData(selected_node);

	UnicodeString unicode_sql1 = L"SELECT MediaTypeId FROM MediaType WHERE Name=\""+node_data->media_type+"\"";

	wchar_t *wchar_sql1 = unicode_sql1.c_str();
	char char_sql1[200];
	wcstombs(char_sql1, wchar_sql1, wcslen(wchar_sql1)+1);

	int result = sqlite3_prepare_v2(Database,char_sql1,-1,&pStatement1,NULL);
	result = sqlite3_step(pStatement1);

	if(result != SQLITE_ROW)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"������ ��������� �������������� ���� �����!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	int media_type_id = reinterpret_cast<int>(sqlite3_column_text(pStatement1, 0));


	sqlite3_stmt *pStatement2;

	UnicodeString unicode_sql2 = "DELETE FROM MediaType WHERE Name=\""+node_data->media_type+"\"";

	wchar_t *wchar_sql2 = unicode_sql2.c_str();
	char char_sql2[100];
	wcstombs(char_sql2, wchar_sql2, wcslen(wchar_sql2)+1);

	result = sqlite3_prepare_v2(Database,char_sql2,-1,&pStatement2,NULL);
	result = sqlite3_step(pStatement2);

	if(result != SQLITE_DONE)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"�������� ������ �� ���������!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	VirtualStringTree1->DeleteNode(selected_node);


	sqlite3_stmt *pStatement3;

	UnicodeString unicode_sql3 = "DELETE FROM Track WHERE MediaTypeId="+UnicodeString((char*)media_type_id);

	wchar_t *wchar_sql3 = unicode_sql3.c_str();
	char char_sql3[100];
	wcstombs(char_sql3, wchar_sql3, wcslen(wchar_sql3)+1);

	result = sqlite3_prepare_v2(Database,char_sql3,-1,&pStatement3,NULL);
	result = sqlite3_step(pStatement3);

    if(result != SQLITE_DONE)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"�������� ��������� ����� �� ���������!",
		(LPCWSTR)L"������",
		MB_ICONERROR
		);
		return;
	}

	sqlite3_finalize(pStatement1);
	sqlite3_finalize(pStatement2);
	sqlite3_finalize(pStatement3);

}
//---------------------------------------------------------------------------

