//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	HINSTANCE hMyDll = LoadLibraryW(L"RAD.dll");
	if (hMyDll == NULL)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"Resource not available\nDo you want to try again?",
		(LPCWSTR)L"Error load DLL",
		MB_ICONWARNING
		);
		return;
	}
	p_group_number f_group_number;
	f_group_number = (p_group_number)GetProcAddress(hMyDll,"f_group_number");

	if (f_group_number == NULL)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"Resource not available\nDo you want to try again?",
		(LPCWSTR)L"Error load function",
		MB_ICONWARNING
		);
		return;
	}

	int group_number = f_group_number();
	Edit1->Text = group_number;

	FreeLibrary(hMyDll);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	HINSTANCE hMyDll = LoadLibraryW(L"RAD.dll");
    if (hMyDll == NULL)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"Resource not available\nDo you want to try again?",
		(LPCWSTR)L"Error load DLL",
		MB_ICONWARNING
		);
		return;
	}
	p_my_surname f_my_surname;
	f_my_surname = (p_my_surname)GetProcAddress(hMyDll,"f_my_surname");

    if (f_my_surname == NULL)
	{
		int msgboxID = MessageBox(
		NULL,
		(LPCWSTR)L"Resource not available\nDo you want to try again?",
		(LPCWSTR)L"Error load function",
		MB_ICONWARNING
		);
		return;
	}

	wchar_t surname[20];
	f_my_surname(surname);
	Edit2->Text = surname;

	FreeLibrary(hMyDll);
}
//---------------------------------------------------------------------------
