﻿#include <iostream>
#include "vs_dll.h"

int main()
{
	int group = f_group_number();
	std::cout << "Group number returned from DLL: " << group << std::endl;

	wchar_t* surname = new wchar_t[20];
	f_my_surname(surname);
	std::wcout << L"My surname returned from DLL: " << surname << std::endl;

	system("pause");
	return 0;
}
