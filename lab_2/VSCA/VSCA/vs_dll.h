#pragma once

#ifdef MYDLL_EXPORTS
#define MYDLL_API __declspec(dllexport)
#else
#define MYDLL_API __declspec(dllimport)
#endif

#include <iostream>

extern "C" MYDLL_API int __stdcall f_group_number();
extern "C" MYDLL_API void __stdcall f_my_surname(wchar_t* surname);
