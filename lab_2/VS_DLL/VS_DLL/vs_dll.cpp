#include "pch.h"
#include "vs_dll.h"

int __stdcall f_group_number()
{
	std::cout << "571227" << std::endl;
	return 571227;
}

void __stdcall f_my_surname(wchar_t* surname)
{
	std::wcout << L"Polina Muratova" << std::endl;

	const wchar_t* my_surname = L"Polina Muratova";
	int i = 0;
	while (my_surname[i] != '\0')
	{
		surname[i] = my_surname[i];
		i++;
	}
	surname[i] = '\0';
}